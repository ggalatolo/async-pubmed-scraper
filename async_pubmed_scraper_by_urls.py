"""
Author: Ilia Zenkov, Gabriele Galatolo
Date: 17/01/2023

This script asynchronously scrapes Pubmed - an open-access database of scholarly research articles -
and saves the data to a DataFrame which is then written to a CSV intended for further processing
This script is capable of scraping a list of keywords asynchronously

Contains the following functions:
    make_header:        Makes an HTTP request header using a random user agent
    get_num_pages:      Finds number of pubmed results pages returned by a keyword search
    extract_by_article: Extracts data from a single pubmed article to a DataFrame
    get_pmids:          Gets PMIDs of all article URLs from a single page and builds URLs to pubmed articles specified by those PMIDs
    build_article_urls: Async wrapper for get_pmids, creates asyncio tasks for each page of results, page by page,
                        and stores article urls in urls: List[string]
    get_article_data:   Async wrapper for extract_by_article, creates asyncio tasks to scrape data from each article specified by urls[]

requires:
    BeautifulSoup4 (bs4)
    PANDAS
    requests
    asyncio
    aiohttp
    nest_asyncio (OPTIONAL: Solves nested async calls in jupyter notebooks)
"""

import argparse
import time
from bs4 import BeautifulSoup
import pandas as pd
import random
import requests
import asyncio
import aiohttp
import socket
import warnings; warnings.filterwarnings('ignore') # aiohttp produces deprecation warnings that don't concern us
#import nest_asyncio; nest_asyncio.apply() # necessary to run nested async loops in jupyter notebooks

# Use a variety of agents for our ClientSession to reduce traffic per agent
# This (attempts to) avoid a ban for high traffic from any single agent
# We should really use proxybroker or similar to ensure no ban
user_agents = [
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",
        "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0",
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",
        "Mozilla/5.0 (X11; CrOS i686 2268.111.0) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6",
        "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.36 Safari/536.5",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
        "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
        "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
        "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
        "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
        "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6",
        "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/19.77.34.5 Safari/537.1",
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.9 Safari/536.5",
        "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.0 Safari/536.3",
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24",
        "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24"
        ]

def make_header():
    '''
    Chooses a random agent from user_agents with which to construct headers
    :return headers: dict: HTTP headers to use to get HTML from article URL
    '''
    # Make a header for the ClientSession to use with one of our agents chosen at random
    headers = {
            'User-Agent':random.choice(user_agents),
            }
    return headers

async def extract_by_article(url):
    '''
    Extracts all data from a single article
    :param url: string: URL to a single article (i.e. root pubmed URL + PMID)
    :return article_data: Dict: Contains all data from a single article
    '''
    conn = aiohttp.TCPConnector(family=socket.AF_INET)
    headers = make_header()
    # Reference our articles DataFrame containing accumulated data for ALL scraped articles
    global articles_data
    async with aiohttp.ClientSession(headers=headers, connector=conn) as session:
        async with semaphore, session.get(url) as response:
            data = await response.text()
            soup = BeautifulSoup(data, "lxml")
            # Get article abstract if exists - sometimes abstracts are not available (not an error)
            try:
                abstract_raw = soup.find('div', {'class': 'abstract-content selected'}).find_all('p')
                # Some articles are in a split background/objectives/method/results style, we need to join these paragraphs
                abstract = ' '.join([paragraph.text.strip() for paragraph in abstract_raw])
            except:
                abstract = 'NO_ABSTRACT'
            # Get references, sometimes they are not available (not an error)
            references = []
            try:
                import re

                all_references = soup.find_all('ol', class_='references-and-notes-list')
                for reference in all_references:
                    references.append(re.sub(' +', ' ', re.sub('\n+', ' ', reference.find('li', class_='skip-numbering').get_text())))
            except:
                references = 'NO_REFERENCE'
            # Get author affiliations - sometimes affiliations are not available (not an error)
            affiliations = []  # list because it would be difficult to split since ',' exists within an affiliation
            try:
                all_affiliations = soup.find('ul', {'class': 'item-list'}).find_all('li')
                for affiliation in all_affiliations:
                    affiliations.append(affiliation.get_text().strip())
            except:
                affiliations = 'NO_AFFILIATIONS'
            # Get article keywords - sometimes keywords are not available (not an error)
            try:
                # We need to check if the abstract section includes keywords or else we may get abstract text
                has_keywords = soup.find_all('strong',{'class':'sub-title'})[-1].text.strip()
                if has_keywords == 'Keywords:':
                    # Taking last element in following line because occasionally this section includes text from abstract
                    keywords = soup.find('div', {'class':'abstract' }).find_all('p')[-1].get_text()
                    keywords = keywords.replace('Keywords:','\n').strip() # Clean it up
                else:
                    keywords = 'NO_KEYWORDS'
            except:
                keywords = 'NO_KEYWORDS'
            try:
                title = soup.find('meta',{'name':'citation_title'})['content'].strip('[]')
            except:
                title = 'NO_TITLE'
            authors = ''    # string because it's easy to split a string on ','
            try:
                for author in soup.find('div',{'class':'authors-list'}).find_all('a',{'class':'full-name'}):
                    authors += author.text + ', '
                # alternative to get citation style authors (no first name e.g. I. Zenkov)
                # all_authors = soup.find('meta', {'name': 'citation_authors'})['content']
                # [authors.append(author) for author in all_authors.split(';')]
            except:
                authors = ('NO_AUTHOR')
            try:
                journal = soup.find('meta',{'name':'citation_journal_title'})['content']
            except:
                journal = 'NO_JOURNAL'
            try:
                date = soup.find('time', {'class': 'citation-year'}).text
            except:
                date = 'NO_DATE'

            # Format data as a dict to insert into a DataFrame
            article_data = {
                'url': url,
                'title': title,
                'authors': authors,
                'abstract': abstract,
                'affiliations': affiliations,
                'journal': journal,
                'keywords': keywords,
                'date': date,
                'references': references
            }
            # Add dict containing one article's data to list of article dicts
            articles_data.append(article_data)

async def get_article_data(urls):
    """
    Async wrapper for extract_by_article to scrape data from each article (url)
    :param urls: List[string]: list of all pubmed urls returned by the search keyword
    :return: None
    """
    tasks = []
    for url in urls:
        if url not in scraped_urls:
            task = asyncio.create_task(extract_by_article(url))
            tasks.append(task)
            scraped_urls.append(url)

    await asyncio.gather(*tasks)


if __name__ == "__main__":
    # Set options so user can choose number of pages and publication date range to scrape, and output file name
    parser = argparse.ArgumentParser(description='Asynchronous PubMed Scraper')
    parser.add_argument('--input', type=str, default='urls.csv',help='Select input file containing urls to scrape Default = "urls.csv".')
    parser.add_argument('--output', type=str, default='articles.csv',help='Choose output file name. Default = "articles.csv".')
    args = parser.parse_args()
    if args.input[-4:] != '.csv': args.input += '.csv' # ensure we save a CSV if user forgot to include format in --output option
    if args.output[-4:] != '.csv': args.output += '.csv' # ensure we save a CSV if user forgot to include format in --output option
    start = time.time()
    # The root pubmed link is used to construct URLs to scrape after PMIDs are retrieved from user specified date range
    root_pubmed_url = 'https://pubmed.ncbi.nlm.nih.gov'

    print(f'\nOpen input file containing urls to scrape...\n')
    urls_list = pd.read_csv(args.input, header=None)

    # Empty list to store all article data as List[dict]; each dict represents data from one article
    # This approach is considerably faster than appending article data article-by-article to a DataFrame
    articles_data = []
    # Empty list to store all article URLs
    urls = []
    # Empty list to store URLs already scraped
    scraped_urls = []

    # We use asyncio's BoundedSemaphore method to limit the number of asynchronous requests
    #    we make to PubMed at a time to avoid a ban (and to be nice to PubMed servers)
    # Higher value for BoundedSemaphore yields faster scraping, and a higher chance of ban. 100-500 seems to be OK.
    semaphore = asyncio.BoundedSemaphore(100)

    # Get and run the loop to build a list of all URLs
    urls_list.apply(lambda row: urls.append(row[0]), axis=1)

    print(f'Scraping initiated for {len(urls)} article URLs\n')
    # Get and run the loop to get article data into a DataFrame from a list of all URLs
    loop = asyncio.get_event_loop()
    loop.run_until_complete(get_article_data(urls))

    # Create DataFrame to store data from all articles
    articles_df = pd.DataFrame(articles_data, columns=['title','abstract','affiliations','authors','journal','date','keywords','url', 'references'])
    print('Preview of scraped article data:\n')
    print(articles_df.head(5))
    # Save all extracted article data to CSV for further processing
    filename = args.output
    articles_df.to_csv(filename, index=False)
    print(f'It took {time.time() - start} seconds to find {len(urls)} articles; {len(scraped_urls)} unique articles were saved to {filename}')

